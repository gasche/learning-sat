  $ dune exec cnf_printer ./cnf/easy/clause_1.cnf
  p cnf 2 3
  1 0
  -1 2 0
  -1 -2 0

  $ dune exec bruteforce ./cnf/easy/clause_1.cnf
  UNSAT

  $ dune exec bruteforce ./cnf/easy/clause_2.cnf
  UNSAT

  $ dune exec bruteforce ./cnf/easy/clause_3.cnf
  SAT: -1 2 -3

  $ dune exec dpll ./cnf/easy/clause_1.cnf
  UNSAT

  $ dune exec dpll ./cnf/easy/clause_2.cnf
  UNSAT

  $ dune exec dpll ./cnf/easy/clause_3.cnf
  SAT: -1 2 -3
