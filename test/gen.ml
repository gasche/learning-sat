open Learning_sat
open Types
open QCheck2

include struct
  open Gen

  let gen_lit ~nb_vars =
    let+ var = int_range 1 nb_vars
    and+ negate = bool
    in Lit (if negate then -var else var)

  let gen_clause ~nb_vars =
    small_array (gen_lit ~nb_vars)

  let gen_cnf ~nb_vars_max ~nb_clauses_max =
    let* nb_vars = int_range 1 nb_vars_max in
    let* nb_clauses = int_range 0 nb_clauses_max in
    let+ clauses = array_repeat nb_clauses (gen_clause ~nb_vars)
    in {
      nb_vars;
      nb_clauses;
      clauses;
    }
end

let gen_small_cnf = gen_cnf ~nb_vars_max:10 ~nb_clauses_max:10

let gen_medium_cnf = gen_cnf ~nb_vars_max:20 ~nb_clauses_max:50

(* about 1s/test for now *)
let count = 10_000

let test_sat_valid =
  Test.make
    ~count
    ~name:"sat valid"
    ~print:string_of_cnf
    gen_medium_cnf @@ fun cnf ->
  match Dpll.solve cnf with
  | None -> true
  | Some v ->
    Eval.eval_cnf v cnf = Ok true

let test_against_bruteforce =
  Test.make
    ~count
    ~name:"against bruteforce"
    ~print:string_of_cnf
    gen_small_cnf @@ fun cnf ->
  let module Valu = Eval.Valuation in
  match Dpll.solve cnf, Bruteforce.solve cnf with
  | None, None -> true
  | Some _, Some _ -> true
  | None, Some _ | Some _, None -> false

let () =
  let exit_code = QCheck_base_runner.run_tests [
    test_sat_valid;
    test_against_bruteforce;
  ] in
  exit exit_code

