open Types

let unit_clauses (cnf : cnf) v : lit list =
  Array.fold_right (fun clause units ->
    let nb_unset = ref 0 in
    let unset = ref (Lit 0) in
    let i = ref 0 in
    let len = Array.length clause in
    while
      !i < len
      && !nb_unset <= 1
    do
      let lit = clause.(!i) in
      if Eval.Valuation.mem_lit v lit then begin
        if Eval.Valuation.get_lit v lit then
          (* hack: exit the check early, without any unit clause *)
          nb_unset := 2
      end else begin
        incr nb_unset;
        unset := lit
      end;
      incr i
    done;
    if !nb_unset = 1
    then !unset :: units
    else units
  ) cnf.clauses []
