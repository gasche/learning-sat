open Types

module Valuation : sig
  type t = private Bool3.t array
  val view : t -> Bool3.t array
  val make : nb_vars:int -> t

  val mem : t -> var -> bool
  val get : t -> var -> Bool3.t
  val set : t -> var -> Bool3.t -> unit

  val mem_lit : t -> lit -> bool
  val get_lit : t -> lit -> bool
  val set_lit : t -> lit -> unit
  val unset_lit : t -> lit -> unit
end

exception Indeterminate of lit

val eval_cnf : Valuation.t -> cnf -> (bool, lit) result
