open Types
module Valu = Eval.Valuation

let rec search cnf v i =
  match Eval.eval_cnf v cnf with
  | Ok true -> Some v
  | Ok false -> None
  | Error _ ->
    Valu.set v (Var i) Bool3.false3;
    match search cnf v (i + 1) with
    | Some _ as result -> result
    | None ->
    Valu.set v (Var i) Bool3.true3;
    match search cnf v (i + 1) with
    | Some _ as result -> result
    | None ->
    Valu.set v (Var i) Bool3.none;
    None

let solve cnf =
  let v = Valu.make ~nb_vars:cnf.nb_vars in
  search cnf v 1
