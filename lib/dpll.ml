open Types
module Valu = Eval.Valuation

let rec search cnf v =
  match Units.unit_clauses cnf v with
  | [] -> decide cnf v
  | units -> propagate_units cnf v units

and propagate_units cnf v units =
  units |> List.iter (fun lit ->
    Valu.set_lit v lit
  );
  match search cnf v with
  | Some _ as result -> result
  | None ->
  units |> List.iter (fun lit ->
    Valu.unset_lit v lit
  );
  None

and decide cnf v =
  match Eval.eval_cnf v cnf with
  | Ok true -> Some v
  | Ok false -> None
  | Error lit ->
    Valu.set_lit v lit;
    match search cnf v with
    | Some _ as result -> result
    | None ->
    Valu.set_lit v (negate_lit lit);
    match search cnf v with
    | Some _ as result -> result
    | None ->
    Valu.unset_lit v lit;
    None

let solve cnf =
  let v = Valu.make ~nb_vars:cnf.nb_vars in
  search cnf v
