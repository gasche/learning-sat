type t = private int

val true3 : t
val false3 : t
val none : t

val of_bool : bool -> t

val is_defined : t -> bool
val is_true : t -> bool
