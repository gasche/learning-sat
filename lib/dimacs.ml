open Types

module Loc = Dolmen_std.Loc

module Term = struct
  type t = lit
  type location = Loc.loc

  let atom ?loc:_ li = Lit li
end

module Statement = struct
  type header = { nb_vars: int; nb_clauses: int }

  type t =
    | Header of header
    | Clause of clause

  type term = Term.t

  type location = Loc.loc

  let p_cnf ?loc:_ nb_vars nb_clauses = Header { nb_vars; nb_clauses }

  let clause ?loc:_ lits =
    Clause (Array.of_list lits)
end

include Dolmen_dimacs.Make(Dolmen_std.Loc)(Term)(Statement)

let process file (stmts: Statement.t list) : cnf =
  let failf fmt =
    Printf.ksprintf failwith
      ("Dimacs parsing [%s]: " ^^ fmt) (Loc.file_name file)
  in
  let headers, clauses =
    stmts |> List.partition_map (function
    | Statement.Header header -> Left header
    | Clause clause -> Right clause)
  in
  let header = match headers with
    | [hd] -> hd
    | [] -> failf "no header clause found"
    | _::_::_ -> failf "several header clauses found"
  in
  let {nb_vars; nb_clauses} : Statement.header = header in
  if (List.length clauses = nb_clauses) then ()
  else failf "unexpected number of clauses";
  (* Note that the variables are in the range [1 .. nb_vars]
     in the DIMACS input, not [0 .. nb_vars - 1] as one might expect.

     The reason to avoid 0 is that it is not possible to represent
     th negated atom as the integer -0. *)
  let valid_lit (Lit lit) = abs lit <= nb_vars in
  let valid_clause cl = Array.for_all valid_lit cl in
  if List.for_all valid_clause clauses then ()
  else failf "variable above nb_vars";
  {
    nb_vars = nb_vars;
    nb_clauses = nb_clauses;
    clauses = Array.of_list clauses;
  }
