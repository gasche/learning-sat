type t = int

let true3 = 1
let false3 = 0
let none = -1

let of_bool b =
  if b then true3 else false3

let is_defined t = (t >= 0)
let is_true t = (t > 0)
