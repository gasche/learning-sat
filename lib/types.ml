type var = Var of int [@@unboxed]

type lit = Lit of int [@@unboxeed] (* negative for negated variables *)

type clause = lit array

type cnf = {
  nb_vars: int;
  nb_clauses: int;
  clauses: clause array;
}

let negate_lit (Lit lit) = Lit (- lit)

let pp_atom ppf (Var v) =
  Printf.bprintf ppf "%d" v
let pp_lit ppf (Lit li) =
  Printf.bprintf ppf "%d" li

let pp_clause ppf lits =
  lits |> Array.iteri (fun i lit ->
    if i > 0 then Printf.bprintf ppf " ";
    pp_lit ppf lit;
  );
  Printf.bprintf ppf " 0"

let pp_cnf ppf cnf =
  Printf.bprintf ppf "p cnf %d %d\n" cnf.nb_vars cnf.nb_clauses;
  cnf.clauses |> Array.iter (fun clause ->
  Printf.bprintf ppf "%a\n" pp_clause clause);
  ()

let string_of_cnf cnf =
  let buf = Buffer.create 42 in
  pp_cnf buf cnf;
  Buffer.contents buf

type 'a var_array = 'a array
let make_var_array ~nb_vars default =
  Array.make (nb_vars + 1) default
