open Types

module Valuation = struct
  type t = Bool3.t var_array

  let view v = v

  let make ~nb_vars =
    make_var_array ~nb_vars Bool3.none

  let mem v (Var i) = Bool3.is_defined v.(i)

  let get v (Var i) = v.(i)
  let set v (Var i) p = v.(i) <- p

  let mem_lit v (Lit lit) =
    Bool3.is_defined v.(abs lit)
  let get_lit v (Lit lit) =
    v.(abs lit) = Bool3.of_bool (lit >= 0)
  let set_lit v (Lit lit) =
    v.(abs lit) <- Bool3.of_bool (lit >= 0)
  let unset_lit v (Lit lit) =
    v.(abs lit) <- Bool3.none
end

let eval_lit v lit = Valuation.get_lit v lit

exception Indeterminate of lit

let eval_clause valuation clause =
  if Array.exists (eval_lit valuation) clause then true
  else begin
    Array.iter (fun lit ->
      if not (Valuation.mem_lit valuation lit)
      then raise (Indeterminate lit)
    ) clause;
    false
  end

let eval_cnf valuation cnf =
  match Array.for_all (eval_clause valuation) cnf.clauses with
  | p -> Ok p
  | exception (Indeterminate lit) -> Error lit
