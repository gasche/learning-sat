open Learning_sat

let usage () =
  Printf.eprintf "Expected usage:\n%s /path/to/dimacs_file.cnf\n%!"
    Sys.argv.(0)

let input_path =
  try Sys.argv.(1) with
  | _ -> usage (); exit 1

let cnf =
  let file, stmts = Dimacs.parse_file input_path in
  Dimacs.process file stmts

module Valu = Eval.Valuation

let () =
  match Bruteforce.solve cnf with
  | None -> print_endline "UNSAT"
  | Some v ->
    Valu.view v
    |> Array.to_seqi
    |> Seq.drop 1
    |> Seq.filter_map (fun (i, b) ->
      if not (Bool3.is_defined b) then None
      else Some (if Bool3.is_true b then i else -i))
    |> Seq.map string_of_int
    |> List.of_seq
    |> String.concat " "
    |> Printf.printf "SAT: %s\n%!"
