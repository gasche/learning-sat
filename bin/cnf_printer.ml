open Learning_sat
open Types

let usage () =
  Printf.eprintf "Expected usage:\n%s /path/to/dimacs_file.cnf\n%!"
    Sys.argv.(0)

let input_path =
  try Sys.argv.(1) with
  | _ -> usage (); exit 1

let cnf =
  let file, stmts = Dimacs.parse_file input_path in
  Dimacs.process file stmts

let () =
  print_string (string_of_cnf cnf);
  flush stdout
